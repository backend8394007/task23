const Joi = require('joi');
const express = require('express');
const app = express();

app.use(express.json());

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`The server is listening on port: ${port}`));

const users = [
    {id: 1, name: 'Ivan'},
    {id: 2, name: 'Tom'},
    {id: 3, name: 'Yan'}
];

app.use((req, res, next) => {
    console.log('Request type:', req.method + '; Request url: ' + req.originalUrl);
    next();
});

app.use('/api/users/:id', (req, res, next) => {
    console.log('Request ID: ', req.params.id);
    next();
});

const middlewareValidError = ((req, res, next) => {
    const {error} = validateUser(req.body);
    if (error) return res.status(400).send(error);
    next();
});

app.get('/api/users', (req, res) =>{
    res.send(users);
});

app.get('/api/users/:id', (req, res) =>{
    const user = users.find(c => c.id === parseInt(req.params.id));
    if (!user) return res.status(404).send('The user was not found with this ID');

    res.send(user);
});

app.post('/api/users', (req, res) =>{
    const {error} = validateUser(req.body);
    if (error) return res.status(400).send(error);

    const user = {
        id: users.length + 1,
        name: req.body.name
    };
    users.push(user);
    res.send(user);
});

app.put('/api/users/:id', middlewareValidError, (req, res) =>{
    const user = users.find(c => c.id === parseInt(req.params.id));
    if (!user) return res.status(404).send('The user was not found with this ID');

    user.name = req.body.name;
    res.send(user);
});

app.delete('/api/users/:id', (req, res) =>{
    const user = users.find(c => c.id === parseInt(req.params.id));
    if (!user) return res.status(404).send('The user was not found with this ID');

    const index = users.indexOf(user);
    users.splice(index, 1);

    res.send(user);
});

const validateUser = (user) =>{
    const schema = Joi.object({
        name: Joi.string().min(3).required()
    });
    return schema.validate(user);
}